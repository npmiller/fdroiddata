Categories:Internet
License:GPLv3
Web Site:http://shubhamchaudhary.in
Source Code:https://github.com/shubhamchaudhary/logmein-android
Issue Tracker:https://github.com/shubhamchaudhary/logmein-android/issues

Auto Name:logmein
Summary:Login to campus networks
Description:
Automatically login to university campus networks.
.

Repo Type:git
Repo:https://github.com/shubhamchaudhary/logmein-android.git

Build:0.4.1,7
    commit=4b716eb5c53a7f920f4bcf24a2

Build:0.5.0,8
    commit=0.5.0

Build:0.5.1,9
    commit=0.5.1

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.5.1
Current Version Code:9

